﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class DeckViewDisplay : MonoBehaviour
{
	private readonly int StartIndex = 172;
	private readonly int EndIndex = 1135;
	private readonly int Height = 190;

	public GameObject CardDisplayPrefab;
	public GameObject CardDisplayOnHoverPrefab;

	public Image DeckViewBackgroundImage;
	private List<GameObject> CardObjects;
	private List<GameObject> CardOnHoverObjects; 

	public void ShowCards(IEnumerable<Card> cards)
	{
		if (CardObjects != null)
		{
			foreach (var cardObject in CardObjects)
			{
				Destroy(cardObject);
			}

			foreach (var cardOnHoverObject in CardOnHoverObjects)
			{
				Destroy(cardOnHoverObject);
			}
		}

		SetupView(cards);

		GetComponent<Canvas>().enabled = true;
	}

	public void SetupView(IEnumerable<Card> cardsToDisplay)
	{
		var cardList = cardsToDisplay.ToList();
		var cardObjectList = new List<GameObject>();
		var cardObjectOnHoverList = new List<GameObject>();

		var spacing = (EndIndex - StartIndex) / (float) cardList.Count;

		for (int i = 0; i < cardList.Count; i++)
		{
			var currentLeft = StartIndex + spacing * i;

			var instantiate = Instantiate(CardDisplayPrefab,
				new Vector3(currentLeft, Height, 0), Quaternion.identity,
				DeckViewBackgroundImage.transform);
			instantiate.GetComponent<CardPreviewDisplay>().CardSetup(cardList[i]);
			instantiate.transform.localPosition =
				new Vector3(currentLeft - 160 - Screen.width / 4, Height - 90 - Screen.height / 4, 0);
			cardObjectList.Add(instantiate);
		}

		for (int i = 0; i < cardList.Count; i++)
		{
			var currentLeft = StartIndex + spacing * i;

			var instantiate = Instantiate(CardDisplayOnHoverPrefab,
				new Vector3(currentLeft, Height, 0), Quaternion.identity,
				DeckViewBackgroundImage.transform);
			instantiate.GetComponent<CardPreviewOnHoverDisplay>().CardSetup(cardList[i]);
			instantiate.transform.localPosition =
				new Vector3(currentLeft - 160 - Screen.width / 4, Height - 90 - Screen.height / 4, 0);
			cardObjectList[i].GetComponent<CardPreviewDisplay>().OnHoverDisplay =
				instantiate.GetComponent<CardPreviewOnHoverDisplay>();
			cardObjectOnHoverList.Add(instantiate);
		}

		CardObjects = cardObjectList;
		CardOnHoverObjects = cardObjectOnHoverList;
	}

	public void CloseView()
	{
		GetComponent<Canvas>().enabled = false;
	}
}
