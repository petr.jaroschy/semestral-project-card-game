﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class CardPreviewDisplay : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
	[HideInInspector]
	public Card Card;

	public TextMeshProUGUI DescriptionText;
	public TextMeshProUGUI CostValueText;
	public TextMeshProUGUI NameText;

	public Image CostIconImage;
	public Image CardImage;

	public CardPreviewOnHoverDisplay OnHoverDisplay;

	public void CardSetup(Card thisCard)
	{
		Card = thisCard;

		NameText.text = Card.CardName;
		DescriptionText.text = string.Format(Card.Description, Card.EffectValue);
		CostValueText.text = Card.Cost.ToString();
		CostIconImage.color = Card.CostType == CostType.Mana ? Color.cyan : Color.green;
		CardImage.sprite = thisCard.Art;
	}

	public void OnPointerEnter(PointerEventData eventData)
	{
		OnHoverDisplay.CardBody.enabled = true;
	}

	public void OnPointerExit(PointerEventData eventData)
	{
		OnHoverDisplay.CardBody.enabled = false;
	}
}
