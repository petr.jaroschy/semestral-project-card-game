﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class TestTreeButtonHandler : MonoBehaviour, IPointerClickHandler
{
    public GameStarter GameStarter;

    public void OnPointerClick(PointerEventData eventData)
    {
        CrossSceneData.Tree = GameStarter.GetRandomTree();
        
        SceneManager.LoadScene("SkillTreeView");
    }
}
