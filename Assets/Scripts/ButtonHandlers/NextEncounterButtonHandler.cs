﻿using UnityEngine;
using UnityEngine.UI;

public class NextEncounterButtonHandler : MonoBehaviour
{
	private const float TransitionTime = 0.5f;

	public TreeManager TreeManager;
    public Text ButtonText;
	
	public void OnColorUpdated(Color color)
	{
		ButtonText.color = color;
	}

	public void DeHighlight()
	{
		iTween.ValueTo(gameObject, iTween.Hash("from", ButtonText.color, "to", Color.gray, "time", TransitionTime, "onupdate", "OnColorUpdated"));
	}

	public void Highlight()
	{
		iTween.ValueTo(gameObject, iTween.Hash("from", ButtonText.color, "to", Color.white, "time", TransitionTime, "onupdate", "OnColorUpdated"));
	}
}
