﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using Debug = System.Diagnostics.Debug;

public class GameManager : MonoBehaviour
{
	public GameObject SpellCardPrefab;
	public GameObject EnemyPrefab;
	public GameObject TargetedSpellCardPrefab;

	public DeckViewDisplay DeckViewDisplay;
	public InfoPanelDisplay InfoPanelDisplay;
	public Canvas DropToPlayCanvas;

	public TextMeshProUGUI DamageNextTurnTmp;
	public GameObject EnemyPanel;
	public GameObject HandPanel;
	public Canvas VictoryScreen;
	public Canvas DefeatScreen;
	public Canvas TutorialScreen;
	public GameObject[] PlayerStats;
	public TurnSimulator TurnSimulator;
	private Thread turnSimulatorThread;

	public Deck Deck;
	[HideInInspector] public List<Card> DiscardPile;
	[HideInInspector] public List<GameObject> EnemiesObjects;
	[HideInInspector] public List<GameObject> CardObjects;
	[HideInInspector] public List<Card> Hand;


	public Card HoveredObject
	{
		set
		{
			if (value == null)
			{
				PlayerStats[1].GetComponent<SettableStat>().PreviewChange(0);
				PlayerStats[2].GetComponent<SettableStat>().PreviewChange(0);
			}
			else
			{
				if (value.CostType == CostType.Mana)
				{
					PlayerStats[1].GetComponent<SettableStat>().PreviewChange(value.Cost);
				}
				else
				{
					PlayerStats[2].GetComponent<SettableStat>().PreviewChange(value.Cost);
				}
			}
		}
	}

	// Use this for initialization
	private void Start()
	{
		TutorialScreen.enabled = CrossSceneData.ShowTutorial;
		CrossSceneData.ShowTutorial = false;
		InfoPanelDisplay.Clear();
		var nextEncounterData = CrossSceneData.NextEncounterData;

		Deck = new Deck
		{
			cards = new Queue<Card>(CrossSceneData.NextEncounterData.Deck)
		};

		Draw();

		var enemyList = new List<GameObject>();

		foreach (var enemy in nextEncounterData.Enemies)
		{
			var item = Instantiate(EnemyPrefab, EnemyPanel.transform);
			enemyList.Add(item);
			item.GetComponent<EnemyDisplay>().CardSetup(enemy);
			item.GetComponent<EnemyDisplay>().GameManager = this;
			item.GetComponent<EnemyDisplay>().InfoPanelDisplay = InfoPanelDisplay;
		}

		EnemiesObjects = enemyList;

		var player = CrossSceneData.Player;
		PlayerStats[0].GetComponent<SettableStat>().MaxValue = player.MaxHealth;
		PlayerStats[0].GetComponent<SettableStat>().SetMeter(player.MaxHealth);
		PlayerStats[1].GetComponent<SettableStat>().MaxValue = player.MaxMana;
		PlayerStats[1].GetComponent<SettableStat>().SetMeter(player.MaxMana);
		PlayerStats[2].GetComponent<SettableStat>().MaxValue = player.MaxEnergy;
		PlayerStats[2].GetComponent<SettableStat>().SetMeter(player.MaxEnergy);

		TurnSimulator = new TurnSimulator(new TurnSimulator.TurnState()
		{
			Health = player.MaxHealth,
			Enemies = nextEncounterData.Enemies.Select(SimulatorEnemy.FromEnemy).ToList(),
			Hand = Hand.ToList(),
			Mana = player.MaxMana,
			Energy = player.MaxEnergy,
			MaxHealth = player.MaxHealth,
			Parent = null,
			MaxMana = player.MaxMana,
			MaxEnergy = player.MaxEnergy
		});
		turnSimulatorThread = new Thread(() => TurnSimulator.Simulate());
		turnSimulatorThread.Start();

		UpdateValues();
		CardCostAvailabilitySet();
	}

	private void WinGame()
	{
		CrossSceneData.Player.PointsToSpend += 1;
		VictoryScreen.enabled = true;
	}

	private void LoseGame()
	{
		DefeatScreen.enabled = true;
	}

	public void ToSkillTree()
	{
		SceneManager.LoadScene("SkillTreeView");
	}

	public void ToMainMenu()
	{
		SceneManager.LoadScene("MainMenu");
	}

	private void Draw()
	{
		var newHand = new List<GameObject>();
		for (int i = 0; i < 5; i++)
		{
			var card = Deck.cards.Dequeue();
			Hand.Add(card);

			if (card.GetType() == typeof(TargetedSpellCard))
			{
				GameObject newCard = Instantiate(TargetedSpellCardPrefab, HandPanel.transform);
				newHand.Add(newCard);
				newCard.GetComponent<TargetedSpellCardDisplay>().CardSetup((TargetedSpellCard) card, this);
				newCard.GetComponent<TargetedSpellCardDisplay>().DrawAnimation(i);
				newCard.GetComponent<TargetedSpellCardDisplay>().InfoPanelDisplay = InfoPanelDisplay;
			}
			else
			{
				GameObject newCard = Instantiate(SpellCardPrefab, HandPanel.transform);
				newHand.Add(newCard);
				newCard.GetComponent<SpellCardDisplay>().CardSetup((SpellCard) card, this);
				newCard.GetComponent<SpellCardDisplay>().DrawAnimation(i);
				newCard.GetComponent<SpellCardDisplay>().InfoPanelDisplay = InfoPanelDisplay;
			}
		}

		CardObjects = newHand;
	}

	private void ResetDeck()
	{
		DiscardPile.AddRange(Deck.cards);
		DiscardPile.Shuffle();
		Deck.cards = new Queue<Card>(DiscardPile);
		DiscardPile = new List<Card>();
	}

	private void DiscardHand()
	{
		while (CardObjects.Count > 0)
		{
			var cardObject = CardObjects[0];
			var spellCard = cardObject.GetComponent<SpellCardDisplay>();

			if (spellCard != null)
			{
				DiscardCard(cardObject, spellCard.Card);
			}

			var targetedSpellCard = cardObject.GetComponent<TargetedSpellCardDisplay>();

			if (targetedSpellCard != null)
			{
				DiscardCard(cardObject, targetedSpellCard.Card);
			}
		}
	}

	public void DiscardCard(GameObject cardObject, Card spellCard)
	{
		CardObjects.Remove(cardObject);
		Hand.Remove(spellCard);

		var display = cardObject.GetComponent<TargetedSpellCardDisplay>();
		if (display != null)
		{
			display.DiscardAnimation();
		}
		else
		{
			cardObject.GetComponent<SpellCardDisplay>().DiscardAnimation();
		}

		DiscardPile.Add(spellCard);
	}

	public void ViewWholeDeck()
	{
		DeckViewDisplay.ShowCards(Deck.cards.Union(DiscardPile).Union(Hand).OrderBy(x => x.Cost));
	}

	public void ViewDeck()
	{
		DeckViewDisplay.ShowCards(Deck.cards.OrderBy(x => x.Cost));
	}

	public void ViewDiscardPile()
	{
		DeckViewDisplay.ShowCards(DiscardPile.OrderBy(x => x.Cost));
	}

	public void EndTurn()
	{
		if (turnSimulatorThread.IsAlive)
		{
			turnSimulatorThread.Abort();
		}

		DiscardHand();

		// Fill resources
		PlayerStats[1].GetComponent<SettableStat>().Fill(9999);
		PlayerStats[2].GetComponent<SettableStat>().Fill(9999);

		// real end turn place

		var damageTaken = EnemyDamage();
		EnemyEndTurnEffects();

		if (Deck.cards.Count < 5)
		{
			ResetDeck();
		}

		Draw();

		InfoPanelDisplay.Show(TurnSimulator.BestTurnState);

		TurnSimulator = new TurnSimulator(new TurnSimulator.TurnState()
		{
			Health = PlayerStats[0].GetComponent<SettableStat>().CurrentValue,
			Enemies = EnemiesObjects.Select(x => SimulatorEnemy.FromEnemy(x.GetComponent<EnemyDisplay>().Enemy)).ToList(),
			Hand = Hand,
			Mana = PlayerStats[1].GetComponent<SettableStat>().CurrentValue,
			Energy = PlayerStats[2].GetComponent<SettableStat>().CurrentValue,
			MaxHealth = PlayerStats[0].GetComponent<SettableStat>().MaxValue,
			MaxMana = PlayerStats[1].GetComponent<SettableStat>().MaxValue,
			MaxEnergy = PlayerStats[2].GetComponent<SettableStat>().MaxValue,
			Parent = null
		});

		turnSimulatorThread = new Thread(() => TurnSimulator.Simulate());
		turnSimulatorThread.Start();

		OnAnyChange();
	}

	private void EnemyEndTurnEffects()
	{
		foreach (var enemyDisplay in EnemiesObjects.Select(x => x.GetComponent<EnemyDisplay>()))
		{
			if (!enemyDisplay.IsDead)
			{
				enemyDisplay.Enemy.EndTurnEffectAction.Invoke(this);
			}
		}
	}

	private int EnemyDamage()
	{
		int result = 0;
		foreach (var enemiesObject in EnemiesObjects)
		{
			if (!enemiesObject.GetComponent<EnemyDisplay>().IsDead)
			{
				var enemyAttack = enemiesObject.GetComponent<EnemyDisplay>().Enemy.Attack;
				result += enemyAttack;
				DamagePlayer(enemyAttack);
			}
		}
		return result;
	}

	private void DamagePlayer(int amount)
	{
		if (!PlayerStats[0].GetComponent<SettableStat>().TrySpend(amount))
		{
			LoseGame();
		}
	}

	#region CardPlayability

	public void CardCostAvailabilitySet()
	{
		var actualMana = PlayerStats[1].GetComponent<SettableStat>().CurrentValue;
		var actualEnergy = PlayerStats[2].GetComponent<SettableStat>().CurrentValue;

		foreach (var card in CardObjects)
		{
			ICardDisplay display = (ICardDisplay) card.GetComponent<TargetedSpellCardDisplay>() ??
			                       card.GetComponent<SpellCardDisplay>();

			if (display.GetCard().CostType == CostType.Energy)
			{
				if (display.GetCard().Cost > actualEnergy)
				{
					DisableCard(card);
				}
				else
				{
					EnableCard(card);
				}
			}
			else if (display.GetCard().CostType == CostType.Mana)
			{
				if (display.GetCard().Cost > actualMana)
				{
					DisableCard(card);
				}
				else
				{
					EnableCard(card);
				}
			}
		}
	}

	private void DisableCard(GameObject card)
	{
		ICardDisplay display = (ICardDisplay) card.GetComponent<TargetedSpellCardDisplay>() ??
		                       card.GetComponent<SpellCardDisplay>();
		display.Disable();

		ICardPlayable playable = (ICardPlayable) card.GetComponent<TargetablePlayable>() ??
		                         card.GetComponent<NonTargetablePlayable>();
		playable.Disable();
	}

	private void EnableCard(GameObject card)
	{
		ICardDisplay display = (ICardDisplay) card.GetComponent<TargetedSpellCardDisplay>() ??
		                       card.GetComponent<SpellCardDisplay>();
		display.Enable();

		ICardPlayable playable = (ICardPlayable) card.GetComponent<TargetablePlayable>() ??
		                         card.GetComponent<NonTargetablePlayable>();
		playable.Enable();
	}

	#endregion

	public void ShowInfoMessage(string infoMessage)
	{
		InfoPanelDisplay.Show(infoMessage);
	}

	public void ShowInfoMessage(Card card, string infoMessage)
	{
		InfoPanelDisplay.Show(card, infoMessage);
	}
	public void ShowInfoMessage(Enemy enemy, string infoMessage)
	{
		InfoPanelDisplay.Show(enemy, infoMessage);
	}

	public void OnAnyChange()
	{
		UpdateValues();
	}

	public void OnDeadEnemy()
	{
		UpdateValues();
	}

	public void OnCardUse(Card card, Enemy enemy = null)
	{
		UpdateValues();
		if (EnemiesObjects.Select(x => x.GetComponent<EnemyDisplay>()).All(x => x.IsDead))
		{
			WinGame();
		}
	}

	public void OnDragNonTargetedSpellCard()
	{
		DropToPlayCanvas.transform.SetAsLastSibling();
	}
	public void OnDropNonTargetedSpellCard()
	{
		DropToPlayCanvas.transform.SetAsFirstSibling();
	}

	public void OnEnemyKilled()
	{
		UpdateValues();
	}

	public void UpdateValues()
	{
		int damageNextTurn = 0;
		foreach (var enemiesObject in EnemiesObjects)
		{
			var enemy = enemiesObject.GetComponent<EnemyDisplay>();

			if (!enemy.IsDead)
			{
				damageNextTurn += enemy.Enemy.Attack;
			}
		}

		DamageNextTurnTmp.text = damageNextTurn.ToString();
		DamageNextTurnTmp.color = damageNextTurn >= PlayerStats[0].GetComponent<SettableStat>().CurrentValue
			? Color.red
			: Color.white;
		PlayerStats[0].GetComponent<SettableStat>().PreviewChange(damageNextTurn);
	}

	public void CloseTutorial()
	{
		TutorialScreen.enabled = false;
	}
}
