﻿using System;
using System.Collections.Generic;

public class EliteEnemy : Enemy
{
	public override IList<EndTurnEffect> EndTurnEffects { get; set; }

	public override EnemyRarity Rarity { get; set; }

	public override Action<GameManager> EndTurnEffectAction { get; set; }
	public List<int> EndTurnEffectsValues { get; set; }

	public override Enemy Clone()
	{
		var newEnemy = CreateInstance<EliteEnemy>();

		newEnemy.Health = Health;
		newEnemy.Attack = Attack;
		newEnemy.ThreatValue = ThreatValue;
		newEnemy.BaseAttack = BaseAttack;
		newEnemy.BaseHealth = BaseHealth;

		newEnemy.name = name;
		newEnemy.CardName = CardName;
		newEnemy.Description = Description;
		newEnemy.Art = Art;
		newEnemy.EndTurnEffectAction = EndTurnEffectAction;
		newEnemy.EndTurnEffects = EndTurnEffects;

		return newEnemy;
	}
}