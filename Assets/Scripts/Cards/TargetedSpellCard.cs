﻿using System;
using UnityEngine;

[CreateAssetMenu(fileName = "New Targeted Spell Card", menuName = "Cards/TargetedSpell")]
public class TargetedSpellCard : Card
{
    public Action<ITargetable> Effect;

    public override CardType CardType
    {
        get
        {
            return CardType.TargetedEffect;
        }
    }

    public override Card Clone()
    {
        var newCard = CreateInstance<TargetedSpellCard>();

        newCard.Description = Description;
        newCard.Effect = Effect;
        newCard.CardType = CardType;
        newCard.BaseCardCost = BaseCardCost;
        newCard.CardEffectType = CardEffectType;
        newCard.BaseEffectType = BaseEffectType;
        newCard.Cost = Cost;
        newCard.CostType = CostType;
        newCard.EffectValue = EffectValue;
        newCard.Art = Art;
        newCard.CardName = CardName;
        newCard.BaseEffectValue = BaseEffectValue;

        return newCard;
    }
}
