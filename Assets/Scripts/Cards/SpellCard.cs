﻿using System;
using UnityEngine;

[CreateAssetMenu(fileName = "New Spell Card", menuName = "Cards/Spell")]
public class SpellCard : Card
{
    public Action<GameManager> Effect;

    public override CardType CardType
    {
        get
        {
            return CardType.NonTargetedEffect;
        }
    }

    public override Card Clone()
    {
        var newCard = CreateInstance<SpellCard>();

        newCard.Description = Description;
        newCard.Effect = Effect;
        newCard.CardType = CardType;
        newCard.BaseCardCost = BaseCardCost;
        newCard.CardEffectType = CardEffectType;
        newCard.BaseEffectType = BaseEffectType;
        newCard.Cost = Cost;
        newCard.CostType = CostType;
        newCard.EffectValue = EffectValue;
        newCard.Art = Art;
        newCard.CardName = CardName;
        newCard.BaseEffectValue = BaseEffectValue;

        return newCard;
    }
}
