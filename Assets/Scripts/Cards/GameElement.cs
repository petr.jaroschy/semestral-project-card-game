﻿using UnityEngine;

public class GameElement : ScriptableObject
{
    public string CardName;
    public string Description;

    public Sprite Art; 
}

