﻿using UnityEngine;

public class Card : GameElement
{
    public virtual CardType CardType { get; protected set; }
    [HideInInspector] public int Cost;
    public CostType CostType;
    [HideInInspector] public EffectType CardEffectType;
    public int BaseEffectValue;
    public int BaseCardCost;
    public EffectType BaseEffectType;
    [HideInInspector] public int EffectValue;

    public void Reset()
    {
        Cost = BaseCardCost;
        EffectValue = BaseEffectValue;
        CardEffectType = BaseEffectType;
    }

    public virtual Card Clone()
    {
        var newCard = CreateInstance<Card>();


        newCard.Description = Description;
        newCard.CardType = CardType;
        newCard.BaseCardCost = BaseCardCost;
        newCard.CardEffectType = CardEffectType;
        newCard.BaseEffectType = BaseEffectType;
        newCard.Cost = Cost;
        newCard.CostType = CostType;
        newCard.EffectValue = EffectValue;
        newCard.Art = Art;
        newCard.CardName = CardName;
        newCard.BaseEffectValue = BaseEffectValue;

        return newCard;
    }

	public virtual CardEntity ToEntity()
	{
		return new CardEntity()
		{
			CardName = CardName,
			Strength = BaseEffectValue
		};
	}
}
