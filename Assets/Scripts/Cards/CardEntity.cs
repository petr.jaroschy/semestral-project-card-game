﻿using System;

[Serializable]
public class CardEntity
{
	public string CardName;
	public int Strength;
}

[Serializable]
class DeckEntity
{
	public CardEntity[] Entities;
}