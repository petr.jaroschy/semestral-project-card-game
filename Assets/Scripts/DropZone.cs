﻿using UnityEngine;
using UnityEngine.EventSystems;

public class DropZone : MonoBehaviour, IDropHandler, IPointerEnterHandler, IPointerExitHandler
{

    public void OnPointerEnter(PointerEventData eventData)
    {
        //Debug.Log("OnPointerEnter");
        if (eventData.pointerDrag == null)
            return;

        NonTargetablePlayable d = eventData.pointerDrag.GetComponent<NonTargetablePlayable>();
        if (d != null)
        {
            d.placeholderParent = this.transform;
        }
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        if (eventData.pointerDrag == null)
            return;

        NonTargetablePlayable d = eventData.pointerDrag.GetComponent<NonTargetablePlayable>();
        if (d != null && d.placeholderParent == this.transform)
        {
            d.placeholderParent = d.parentToReturnTo;
        }
    }

    public void OnDrop(PointerEventData eventData)
    {
        Debug.Log(eventData.pointerDrag.name + " was dropped on " + gameObject.name);

        NonTargetablePlayable d = eventData.pointerDrag.GetComponent<NonTargetablePlayable>();
        if (d != null)
        {
            d.parentToReturnTo = this.transform;
        }

    }
}
