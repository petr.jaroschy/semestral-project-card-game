﻿using System.Collections.Generic;

public class NodeTree : ITree
{
	private Node root;

	public NodeTree(Node root)
	{
		this.root = root;
	}

	public INode Root
	{
		get { return root; }
	}

	public IEnumerable<INode> GetSelectedNodes()
	{
		var nodeQueue = new Queue<INode>(root.ConnectedTo);
		var selectedNodes = new List<INode>();

		while (nodeQueue.Count > 0)
		{
			var nextNodeInQueue = nodeQueue.Dequeue();
			switch (nextNodeInQueue.NodeState)
			{
				case NodeState.Bought:
					if (nextNodeInQueue.ConnectedTo == null)
					{
						break;
					}

					foreach (var node in nextNodeInQueue.ConnectedTo)
					{
						nodeQueue.Enqueue(node);
					}

					break;
				case NodeState.Selected:
					if (nextNodeInQueue.ConnectedTo == null)
					{
						break;
					}

					selectedNodes.Add(nextNodeInQueue);
					foreach (var node in nextNodeInQueue.ConnectedTo)
					{
						nodeQueue.Enqueue(node);
					}

					break;
			}
		}

		return selectedNodes;
	}

	public IEnumerable<INode> GetBoughtNodes()
	{
		var nodeQueue = new Queue<INode>(root.ConnectedTo);
		var boughtNodes = new List<INode>();

		while (nodeQueue.Count > 0)
		{
			var nextNodeInQueue = nodeQueue.Dequeue();
			if (nextNodeInQueue.NodeState == NodeState.Bought)
			{
				boughtNodes.Add(nextNodeInQueue);
				foreach (var node in nextNodeInQueue.ConnectedTo)
				{
					nodeQueue.Enqueue(node);
				}
			}
		}

		return boughtNodes;
	}

	public List<Node> GetAllNodes()
	{
		var nodeQueue = new Queue<Node>(root.Children);
		var nodes = new List<Node>();

		while (nodeQueue.Count > 0)
		{
			var nextNodeInQueue = nodeQueue.Dequeue();
			nodes.Add(nextNodeInQueue);
			foreach (var node in nextNodeInQueue.Children)
			{
				nodeQueue.Enqueue(node);
			}
		}

		return nodes;
	}
}