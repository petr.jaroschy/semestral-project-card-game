﻿using System;

[Serializable]
public class NodeEntity
{
	public int Id;
	public int EffectValue;
	public NodeTarget[] Targets;
	public int[] ChildrenIds;
	public string Description;
	public string SpriteName;
}

[Serializable]
public class NodeEntities
{
	public NodeEntity[] Entities;
}
