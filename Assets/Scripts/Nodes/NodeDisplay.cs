﻿using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class NodeDisplay : MonoBehaviour, IPointerEnterHandler,IPointerExitHandler,IPointerClickHandler
{
	private const float TransitionTime = 0.5f;

	[HideInInspector]
    public TreeManager TreeManager;
    [HideInInspector]
    public INode Node;

    public Image NodeIconImage;
	public TextMeshProUGUI NodePowerText;
    private bool _isSelected;

    public Image TooltipImage;
    public Text TooltipText;

    public void NodeSetup(INode node)
    {
        Node = node;

        if (node.Sprite != null)
        {
            NodeIconImage.sprite = node.Sprite; 
        }

	    NodePowerText.text = node.NodePower.ToString();

        TooltipText.text = string.Format(node.DescriptionString, node.NodePower);
        if (Node.NodeState == NodeState.None)
        {
            DeHighlightNode();
        }
    }

    public void SetTooltipVisibility(bool toSet)
    {
        TooltipImage.enabled = toSet;
        TooltipText.enabled = toSet;
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        SetTooltipVisibility(true);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        SetTooltipVisibility(false);
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if (Node.NodeState == NodeState.Bought)
        {
            return;
        }
        if (!_isSelected)
        {
            SelectNodeToPurchase(); 
        }
        else
        {
            DeselectNodeToPurchase();
        }
    }

    private void DeselectNodeToPurchase()
    {
        if (TreeManager.IsNodeDeSelectable(Node))
        {
            TreeManager.UnSpend();
            DeHighlightNode();
            _isSelected = false;
            Node.NodeState = NodeState.None;
			TreeManager.OnAnyChange();
        }
    }

    private void SelectNodeToPurchase()
    {
        if (TreeManager.IsNodePurchasable(Node))
        {
            if (TreeManager.TrySpend())
            {
                HighlightNode();
                _isSelected = true;
	            Node.NodeState = NodeState.Selected;
	            TreeManager.OnAnyChange();
			}
        }
    }

	public void OnColorUpdated(Color color)
	{
		NodeIconImage.color = color;
	}

    private void DeHighlightNode()
    {
		iTween.ValueTo(gameObject, iTween.Hash("from", NodeIconImage.color, "to", Color.gray, "time", TransitionTime, "onupdate", "OnColorUpdated"));
    }

    private void HighlightNode()
    {
	    iTween.ValueTo(gameObject, iTween.Hash("from", NodeIconImage.color, "to", Color.yellow, "time", TransitionTime, "onupdate", "OnColorUpdated"));
    }
}
