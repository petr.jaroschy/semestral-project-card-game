﻿using System.Collections.Generic;

public class Deck
{
    public Queue<Card> cards;
    
    public int Size
    {
        get { return cards.Count; }
    }
}
