﻿using UnityEngine;
using UnityEngine.EventSystems;

public class DropToPlayZone : MonoBehaviour, IDropHandler
{
    public GameManager GameManager;

    public void OnDrop(PointerEventData eventData)
    {
        Debug.Log("BackgroundDropZone Drop");
        var spell = eventData.pointerDrag.GetComponent<SpellCardDisplay>();
	    eventData.pointerDrag.GetComponent<NonTargetablePlayable>().BeingPlayed = true;

		if (spell != null)
        {
            if (spell.Card.CostType == CostType.Energy)
            {
                if (GameManager.PlayerStats[2].GetComponent<SettableStat>().TrySpend(spell.Card.Cost))
                {
	                eventData.pointerDrag.GetComponent<NonTargetablePlayable>().BeingPlayed = true;
					spell.Card.Effect.Invoke(GameManager);
					GameManager.OnCardUse(spell.Card);
                    GameManager.DiscardCard(eventData.pointerDrag, spell.Card);
                }
                else
                {
                    GameManager.ShowInfoMessage("Not enough energy!");
                }
            }
            else
            {
                if (GameManager.PlayerStats[1].GetComponent<SettableStat>().TrySpend(spell.Card.Cost))
                {
	                eventData.pointerDrag.GetComponent<NonTargetablePlayable>().BeingPlayed = true;
					spell.Card.Effect.Invoke(GameManager);
	                GameManager.OnCardUse(spell.Card);
					GameManager.DiscardCard(eventData.pointerDrag, spell.Card);
                }
                else
                {
                    GameManager.ShowInfoMessage("Not enough mana!");
                }
            }
        }

        if (eventData.pointerDrag.GetComponent<NonTargetablePlayable>().placeholder != null)
        {
            Destroy(eventData.pointerDrag.GetComponent<NonTargetablePlayable>().placeholder); 
        }
    }
}
