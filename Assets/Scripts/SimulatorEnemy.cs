﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Random = System.Random;

public class SimulatorEnemy
{
	public string CardName;
	public string Description;

	public Sprite Art;
	public int Attack;
	public int Health;
	public int BaseAttack;
	public int BaseHealth;
	public int ThreatValue;

	public virtual EnemyRarity Rarity
	{
		get { return EnemyRarity.Normal; }
		set { }
	}

	public virtual IList<EndTurnEffect> EndTurnEffects
	{
		get { return new List<EndTurnEffect>(); }
		set { }
	}

	public void Reset()
	{
		Attack = BaseAttack;
		Health = BaseHealth;
	}

	public virtual SimulatorEnemy Clone()
	{
		var newEnemy = new SimulatorEnemy();

		newEnemy.Health = Health;
		newEnemy.Attack = Attack;
		newEnemy.ThreatValue = ThreatValue;
		newEnemy.BaseAttack = BaseAttack;
		newEnemy.BaseHealth = BaseHealth;

		newEnemy.CardName = CardName;
		newEnemy.Description = Description;
		newEnemy.Art = Art;


		return newEnemy;
	}

	public SimulatorEliteEnemy GetEliteVersion()
	{
		var newEnemy = new SimulatorEliteEnemy();

		newEnemy.Health = Health;
		newEnemy.Attack = Attack;
		newEnemy.ThreatValue = ThreatValue;
		newEnemy.BaseAttack = BaseAttack;
		newEnemy.BaseHealth = BaseHealth;

		newEnemy.CardName = CardName;
		newEnemy.Description = Description;
		newEnemy.Art = Art;

		Random r = new Random();
		newEnemy.Rarity = r.Next(4) == 3 ? EnemyRarity.Rare : EnemyRarity.Magic;


		var newEnemyEndTurnEffects = newEnemy.Rarity == EnemyRarity.Magic
			? new List<EndTurnEffect> {(EndTurnEffect) r.Next(5)}
			: new List<EndTurnEffect> {(EndTurnEffect) r.Next(5), (EndTurnEffect) r.Next(5)};
		newEnemy.EndTurnEffects = newEnemyEndTurnEffects;
		var newEnemyEndTurnEffectsValues = newEnemy.Rarity == EnemyRarity.Magic
			? new List<int> {r.Next(4)}
			: new List<int> {r.Next(4), r.Next(4)};
		newEnemy.EndTurnEffectsValues = newEnemyEndTurnEffectsValues;

		return newEnemy;
	}

	public static SimulatorEnemy FromEnemy(Enemy enemy)
	{
		return new SimulatorEnemy()
		{
			Health = enemy.Health,
			Attack = enemy.Attack,
			EndTurnEffects = enemy.EndTurnEffects,
			Rarity = enemy.Rarity,
			CardName = enemy.CardName,
			ThreatValue = enemy.ThreatValue,
			Art = enemy.Art,
			Description = enemy.Description,
			BaseHealth = enemy.BaseHealth,
			BaseAttack = enemy.BaseAttack
		};
	}
}