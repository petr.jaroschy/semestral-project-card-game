﻿using System;

[Serializable]
class SaveGameEntity
{
	public string TreeName;
	public string DeckName;
	public string[] EncounterNames;
}
