﻿using System.Collections.Generic;

public interface ITree
{
    INode Root { get; }
    IEnumerable<INode> GetSelectedNodes();
    IEnumerable<INode> GetBoughtNodes();
}
