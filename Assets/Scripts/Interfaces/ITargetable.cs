﻿public interface ITargetable
{
    int RemainingHealth { get; }
    int Damage(int damage);
    int ReduceDamage(int amount);
}
