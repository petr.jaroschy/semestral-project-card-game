﻿public interface ICardDisplay
{
	void Dim();
	void LightUp();
	void Disable();
	void Enable();
	Card GetCard();
}