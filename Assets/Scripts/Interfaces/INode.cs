﻿using System.Collections.Generic;
using UnityEngine;

public interface INode
{
    IEnumerable<INode> ConnectedTo { get; }
    INode Parent { get; set; }
    void ApplyEffectToCards(IEnumerable<Card> cards);
	IEnumerable<NodeTarget> Targets { get; set; }
    NodeState NodeState { get; set; }
    Sprite Sprite { get; set; }
    string DescriptionString { get; set; }
    int NodePower { get; set; }
}