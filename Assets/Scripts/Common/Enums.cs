﻿public enum CardType
{
    /// <summary>
    /// One time targeted effect
    /// </summary>
    TargetedEffect,

    /// <summary>
    /// One time non targeted effect
    /// </summary>
    NonTargetedEffect
}

public enum EndTurnEffect
{
	HealAll,
	HealSelf,
	ManaDrain,
	EnergyDrain,
	DeBuffCards
}

public enum EnemyRarity
{
	Normal,
	Magic,
	Rare
}

public enum CostType
{
    Mana,
    Energy
}

public enum EffectType
{
    Heal,
    Damage,
    ReduceDamage,
    ManaFill,
    EnergyFill
}

public enum NodeTarget
{
    SingleTarget,
    MultiTarget,
    Energy,
    Mana,
    Heal,
    Damage,
    DamageReduction,
    All
}

public enum NodeState
{
    Bought,
    Selected,
    None
}
