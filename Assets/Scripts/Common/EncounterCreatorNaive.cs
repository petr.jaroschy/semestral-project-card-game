﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class EncounterCreatorNaive : IEncounterCreator
{
    public EncounterData CreateEncounterData(ITree tree, List<Card> deck, int encounterNumber)
    {
	    var possibleEnemies = CrossSceneData.PossibleEnemies;
		var toReturn = new EncounterData();

        foreach (var card in deck)
        {
            card.Reset();
        }

        foreach (var enemy in possibleEnemies)
        {
            enemy.Reset();
        }

        foreach (var node in tree.GetBoughtNodes().Distinct())
        {
            node.ApplyEffectToCards(deck);
        }

        deck.Shuffle();

        UpdateCardActions(deck);

        toReturn.Deck = deck;

        foreach (var enemy in possibleEnemies)
        {
            enemy.Attack *= encounterNumber;
            enemy.Health *= encounterNumber;
        }

        var combinedThreat = 0;
        var enemyList = new List<Enemy>();

		// Choosing enemies
        while (combinedThreat < 40 && enemyList.Count < 7)
        {
	        enemyList.Add(Random.Range(0, 5) == 0
		        ? possibleEnemies[Random.Range(0, possibleEnemies.Count)].GetEliteVersion()
		        : possibleEnemies[Random.Range(0, possibleEnemies.Count)].Clone());
	        combinedThreat = enemyList.Sum(x => x.ThreatValue);
        }

        toReturn.Enemies = enemyList.ToList();
        return toReturn;
    }

    public static void UpdateCardActions(IEnumerable<Card> cards)
    {
        foreach (var possibleSpellCard in cards)
        {
            if (possibleSpellCard.GetType() == typeof(SpellCard))
            {
                SpellCard spellCard = (SpellCard)possibleSpellCard;
                switch (possibleSpellCard.CardEffectType)
                {
                    case EffectType.Heal:
                        spellCard.Effect = Effects.HealAction(possibleSpellCard.EffectValue);
                        break;
                    case EffectType.Damage:
                        spellCard.Effect = Effects.DamageAllAction(possibleSpellCard.EffectValue);
                        break;
                    case EffectType.ReduceDamage:
                        spellCard.Effect = Effects.ReduceDamageAllAction(possibleSpellCard.EffectValue);
                        break;
                    case EffectType.ManaFill:
                        spellCard.Effect = Effects.ManaGainAction(possibleSpellCard.EffectValue);
                        break;
                    case EffectType.EnergyFill:
                        spellCard.Effect = Effects.EnergyGainAction(possibleSpellCard.EffectValue);
                        break;
                } 
            }
            else
            {
                TargetedSpellCard targetedSpellCard = (TargetedSpellCard)possibleSpellCard;
                switch (targetedSpellCard.CardEffectType)
                {
                    case EffectType.Heal:
                        break;
                    case EffectType.Damage:
                        targetedSpellCard.Effect = Effects.DamageAction(possibleSpellCard.EffectValue);
                        break;
                    case EffectType.ReduceDamage:
                        targetedSpellCard.Effect = Effects.ReduceDamageAction(possibleSpellCard.EffectValue);
                        break;
                    case EffectType.ManaFill:
                        break;
                    case EffectType.EnergyFill:
                        break;
                }
            }

        }
    }
}
