﻿using System;
using System.Linq;

static class EndTurnEffects
{
	public static Action<GameManager> GetEndTurnEffect(Enemy enemy, EndTurnEffect effectType, int effectStrength)
	{
		switch (effectType)
		{
			case EndTurnEffect.HealAll:
				return x =>
				{
					foreach (var enemyDisplay in x.EnemiesObjects.Select(y => y.GetComponent<EnemyDisplay>()))
					{
						enemyDisplay.Heal(effectStrength);
					}
				};
			case EndTurnEffect.HealSelf:
				return x =>
				{
					x.EnemiesObjects.Select(y => y.GetComponent<EnemyDisplay>())
						.Single(y => y.Enemy == enemy).Heal(effectStrength);
				};
			case EndTurnEffect.ManaDrain:
				return x =>
				{
					x.PlayerStats[1].GetComponent<SettableStat>().TrySpend(effectStrength);
				};
			case EndTurnEffect.EnergyDrain:
				return x =>
				{
					x.PlayerStats[2].GetComponent<SettableStat>().TrySpend(effectStrength * 2);
				};
			case EndTurnEffect.DeBuffCards:
				return x =>
				{
					var cards = x.DiscardPile.Union(x.Deck.cards).ToList();
					foreach (var card in cards)
					{
						card.EffectValue -= effectStrength;
					}
					EncounterCreatorNaive.UpdateCardActions(cards);
				};
			default:
				throw new ArgumentOutOfRangeException("effectType", effectType, null);
		}
	}
}
