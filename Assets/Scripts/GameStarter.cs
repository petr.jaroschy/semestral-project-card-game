﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using Random = UnityEngine.Random;

public class GameStarter : MonoBehaviour
{
	private const string TreeJsonLocation = "./Saved/Trees/";
	private const string DeckJsonLocation = "./Saved/Decks/";
	private const string GameJsonLocation = "./Saved/Game/";
	private const string EnemySetsJsonLocation = "./Saved/EnemySets/";
	public List<Enemy> PossibleEnemies;
	public SpellCard[] PossibleSpellCards;
	public TargetedSpellCard[] PossibleTargetedSpellCards;
	public Node[] PossibleNodes;
	public Node DefaultRootNode;
	public IEncounterCreator EncounterCreator = new EncounterCreatorNaive();
	public Node RootNode;
	public Canvas LoadGameCanvas;
	public GameObject SavePrefab;
	public List<Sprite> NodeSprites;
	public List<Sprite> EnemySprites;

	private void Start()
	{
		InitSaved();
		CrossSceneData.ShowTutorial = true;
		foreach (var possibleSpellCard in PossibleSpellCards)
		{
			possibleSpellCard.Reset();
			switch (possibleSpellCard.CardEffectType)
			{
				case EffectType.Heal:
					possibleSpellCard.Effect = Effects.HealAction(possibleSpellCard.EffectValue);
					break;
				case EffectType.Damage:
					possibleSpellCard.Effect = Effects.DamageAllAction(possibleSpellCard.EffectValue);
					break;
				case EffectType.ReduceDamage:
					possibleSpellCard.Effect = Effects.ReduceDamageAllAction(possibleSpellCard.EffectValue);
					break;
				case EffectType.ManaFill:
					possibleSpellCard.Effect = Effects.ManaGainAction(possibleSpellCard.EffectValue);
					break;
				case EffectType.EnergyFill:
					possibleSpellCard.Effect = Effects.EnergyGainAction(possibleSpellCard.EffectValue);
					break;
			}
		}

		foreach (var card in PossibleTargetedSpellCards)
		{
			card.Reset();
			switch (card.CardEffectType)
			{
				case EffectType.Heal: break;
				case EffectType.Damage:
					card.Effect = Effects.DamageAction(card.EffectValue);
					break;
				case EffectType.ReduceDamage:
					card.Effect = Effects.ReduceDamageAction(card.EffectValue);
					break;
				case EffectType.ManaFill: break;
				case EffectType.EnergyFill: break;
			}
		}

		CrossSceneData.PossibleEnemies = PossibleEnemies;
		foreach (var enemy in PossibleEnemies) enemy.Reset();
		foreach (var node in PossibleNodes)
			node.EffectAction = NodeEffects.ChangeFlatEffectAction(node.NodePower, node.NodeTargets);
		CrossSceneData.EncounterNumber = 1;
		var loadTree = LoadTree("Tree1.json");
		InitPlayer();
		InitDefaultTree(loadTree);
		InitDefaultTree(DefaultRootNode);
		loadTree.NodeState = NodeState.Bought;
		DefaultRootNode.NodeState = NodeState.Bought;

		// todo testing
		RootNode = loadTree;
	}

	private void InitSaved()
	{
		var files = Directory.GetFiles(GameJsonLocation);
		float left = -305f;
		float top = 285f;
		foreach (var file in files)
		{
			var instantiate = Instantiate(SavePrefab, Vector3.zero, Quaternion.identity,
				LoadGameCanvas.transform.GetChild(0));
			instantiate.transform.localPosition = new Vector3(left, top);
			instantiate.GetComponent<SaveDisplay>().Setup(file.Split('/').Last().Replace(".json",""), file.Split('/').Last());
			top -= 100;
		}
	}

	private void InitDefaultTree(Node node)
	{
		node.EffectAction = NodeEffects.ChangeFlatEffectAction(node.NodePower, node.NodeTargets);
		node.NodeState = NodeState.None;
		if (node.ConnectedTo == null) return;
		foreach (var node1 in node.Children)
		{
			node1.Parent = node;
			InitDefaultTree(node1);
		}
	}

	public List<Card> LoadDeck(string deckName)
	{
		var reader = File.OpenText(DeckJsonLocation + deckName);
		var jsonStrings = reader.ReadToEnd();
		var cardEntities = JsonUtility.FromJson<DeckEntity>(jsonStrings).Entities;
		var deck = cardEntities.Select(FromEntity).ToList();
		return deck;
	}

	public Node LoadTree(string treeName)
	{
		var reader = File.OpenText(TreeJsonLocation + treeName);
		var jsonStrings = reader.ReadToEnd();
		var nodeEntities = JsonUtility.FromJson<NodeEntities>(jsonStrings).Entities;
		var nodes = nodeEntities.Select(Node.FromEntity).ToList();
		foreach (var node in nodes)
		{
			node.Children = nodes.Where(x => nodeEntities.First(y => y.Id == node.Id).ChildrenIds.Contains(x.Id))
				.ToArray();
			foreach (var nodeChild in node.Children)
			{
				nodeChild.Parent = node;
			}
		}

		foreach (var node in nodes)
		{
			NodeEntity nodeEntity = nodeEntities.First(y => y.Id == node.Id);
			if (!string.IsNullOrEmpty(nodeEntity.SpriteName))
			{
				node.Sprite =
						NodeSprites.FirstOrDefault(x => x.name == nodeEntity.SpriteName) ??
						Sprite.Create(Texture2D.whiteTexture, Rect.zero, Vector2.zero); 
			}
			else
			{
				switch (node.NodeTargets.First())
				{
					case NodeTarget.SingleTarget:
						node.Sprite =
							NodeSprites.FirstOrDefault(x => x.name == "headshot") ??
							Sprite.Create(Texture2D.whiteTexture, Rect.zero, Vector2.zero);
						break;
					case NodeTarget.MultiTarget:
						node.Sprite =
							NodeSprites.FirstOrDefault(x => x.name == "wind-slap") ??
							Sprite.Create(Texture2D.whiteTexture, Rect.zero, Vector2.zero);
						break;
					case NodeTarget.Energy:
						node.Sprite =
							NodeSprites.FirstOrDefault(x => x.name == "hooded-assassin") ??
							Sprite.Create(Texture2D.whiteTexture, Rect.zero, Vector2.zero);
						break;
					case NodeTarget.Mana:
						node.Sprite =
							NodeSprites.FirstOrDefault(x => x.name == "robe") ??
							Sprite.Create(Texture2D.whiteTexture, Rect.zero, Vector2.zero);
						break;
					case NodeTarget.Heal:
						node.Sprite =
							NodeSprites.FirstOrDefault(x => x.name == "broken-heart") ??
							Sprite.Create(Texture2D.whiteTexture, Rect.zero, Vector2.zero);
						break;
					case NodeTarget.Damage:
						node.Sprite =
							NodeSprites.FirstOrDefault(x => x.name == "broadsword") ??
							Sprite.Create(Texture2D.whiteTexture, Rect.zero, Vector2.zero);
						break;
					case NodeTarget.DamageReduction:
						node.Sprite =
							NodeSprites.FirstOrDefault(x => x.name == "shattered-sword") ??
							Sprite.Create(Texture2D.whiteTexture, Rect.zero, Vector2.zero);
						break;
					case NodeTarget.All:
						node.Sprite =
							NodeSprites.FirstOrDefault(x => x.name == "mighty-force") ??
							Sprite.Create(Texture2D.whiteTexture, Rect.zero, Vector2.zero);
						break;
					default:
						node.Sprite =
							NodeSprites.FirstOrDefault(x => x.name == "embrassed-energy") ??
							Sprite.Create(Texture2D.whiteTexture, Rect.zero, Vector2.zero);
						break;
				}
			}

			node.EffectAction = NodeEffects.ChangeFlatEffectAction(node.NodePower, node.NodeTargets);
		}

		var root = nodes.First(x => x.Parent == null);
		root.NodeState = NodeState.Bought;
		return root;
	}

	public List<List<Enemy>> LoadEncounters(string[] encountersPath)
	{
		List<List<Enemy>> encounters = new List<List<Enemy>>();
		foreach (var path in encountersPath)
		{
			var newList = new List<Enemy>();
			var reader = File.OpenText(EnemySetsJsonLocation + path);
			var jsonStrings = reader.ReadToEnd();
			var enemySetEntity = JsonUtility.FromJson<EnemySetEntity>(jsonStrings);
			newList.AddRange(enemySetEntity.Entities.Select(FromEntity));
			encounters.Add(newList);
		}

		return encounters;
	}

	public void Load(string savePath)
	{
		var reader = File.OpenText(GameJsonLocation + savePath);
		var jsonStrings = reader.ReadToEnd();
		var saveGameEntity = JsonUtility.FromJson<SaveGameEntity>(jsonStrings);
		var root = LoadTree(saveGameEntity.TreeName);
		var deck = LoadDeck(saveGameEntity.DeckName);
		var encounters = LoadEncounters(saveGameEntity.EncounterNames);
		deck.Shuffle();
		CrossSceneData.EncounterEnemySets = encounters;
		CrossSceneData.Tree = new NodeTree(root);
		CrossSceneData.NextEncounterData = new EncounterData {Deck = deck, Enemies = encounters[0]};
		SceneManager.LoadScene("Encounter");
	}

	public ITree GetRandomTree()
	{
		var root = ScriptableObject.CreateInstance<Node>();
		root.NodeState = NodeState.Bought;
		root.Description = "You are the warrior.";
		var tree = new NodeTree(root);
		var firstGen = new[]
		{
			(Node) PossibleNodes[Random.Range(0, PossibleNodes.Length)].Clone(),
			(Node) PossibleNodes[Random.Range(0, PossibleNodes.Length)].Clone()
		};
		root.Children = firstGen;
		foreach (var node in firstGen)
		{
			node.Parent = root;
			node.Children = new[]
			{
				(Node) PossibleNodes[Random.Range(0, PossibleNodes.Length)].Clone(),
				(Node) PossibleNodes[Random.Range(0, PossibleNodes.Length)].Clone()
			};
			foreach (var secondNode in node.ConnectedTo) secondNode.Parent = node;
		}

		return tree;
	}

	public void SetNextEncounter(int encounterNumber)
	{
		CrossSceneData.NextEncounterData = EncounterCreator.CreateEncounterData(CrossSceneData.Tree,
			CrossSceneData.NextEncounterData.Deck, encounterNumber);
	}

	public void StartNewGameRandomDeck()
	{
		var cardList = new List<Card>();
		for (var i = 0; i < 15; i++)
			cardList.Add(Random.Range(0, 2) == 1
				? PossibleSpellCards[Random.Range(0, PossibleSpellCards.Length)].Clone()
				: PossibleTargetedSpellCards[Random.Range(0, PossibleTargetedSpellCards.Length)].Clone());
		CrossSceneData.NextEncounterData.Deck = cardList.ToList();
		CrossSceneData.Tree = new NodeTree(RootNode);
		SetNextEncounter(1);
		InitPlayer();
		StartEncounter();
	}

	public void Randomize()
	{
		CrossSceneData.NextEncounterData = new EncounterData();
		var enemyList = new List<Enemy>();
		var enemyCount = Random.Range(3, 6);
		for (var i = 0; i < enemyCount; i++)
		{
			var index = Random.Range(0, PossibleEnemies.Count);
			enemyList.Add(PossibleEnemies[index].Clone());
		}

		CrossSceneData.NextEncounterData.Enemies = enemyList.ToList();
		var cardList = new List<Card>();
		for (var i = 0; i < 15; i++)
			cardList.Add(Random.Range(0, 2) == 1
				? PossibleSpellCards[Random.Range(0, PossibleSpellCards.Length)].Clone()
				: PossibleTargetedSpellCards[Random.Range(0, PossibleTargetedSpellCards.Length)].Clone());
		CrossSceneData.NextEncounterData.Deck = cardList.ToList();
		InitPlayer();
	}

	public void ShowLoadGame()
	{
		LoadGameCanvas.enabled = true;
	}

	private static void InitPlayer()
	{
		CrossSceneData.Player = new Player {MaxHealth = 50, MaxMana = 10, MaxEnergy = 20};
	}

	public void StartEncounter()
	{
		SceneManager.LoadScene("Encounter");
	}

	public void QuitGame()
	{
		Application.Quit();
	}

	private Card FromEntity(CardEntity entity)
	{
		var cards = PossibleSpellCards.Cast<Card>().Concat(PossibleTargetedSpellCards).ToList();
		var card = cards.First(x => x.CardName == entity.CardName).Clone();
		card.BaseEffectValue = entity.Strength;
		card.EffectValue = entity.Strength;
		return card;
	}

	private Enemy FromEntity(EnemyEntity entity)
	{
		var enemy = ScriptableObject.CreateInstance<Enemy>();
		enemy.CardName = entity.Name;
		enemy.Health = entity.Defense;
		enemy.Attack = entity.Attack;
		enemy.BaseAttack = entity.Attack;
		enemy.BaseHealth = entity.Defense;
		enemy.ThreatValue = (int) (20 * (entity.Attack / Math.Sqrt(entity.Defense)));
		enemy.Art = EnemySprites.First(x => x.name == entity.SpriteName);
		return enemy;
	}
}
